%global _empty_manifest_terminate_build 0
Name:		python-aiohttp
Version:	3.7.4
Release:	1
Summary:	Async http client/server framework (asyncio)
License:	Apache 2
URL:		https://github.com/aio-libs/aiohttp
Source0:	https://files.pythonhosted.org/packages/99/f5/90ede947a3ce2d6de1614799f5fea4e93c19b6520a59dc5d2f64123b032f/aiohttp-3.7.4.post0.tar.gz

BuildRequires:	python3-attrs
BuildRequires:	python3-chardet
BuildRequires:	python3-multidict
BuildRequires:	python3-async-timeout
BuildRequires:	python3-yarl
BuildRequires:	python3-typing-extensions
BuildRequires:	python3-cchardet

%description
Async http client/server framework (asyncio).

%package -n python3-aiohttp
Summary:	Async http client/server framework (asyncio)
Provides:	python-aiohttp
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-cffi
BuildRequires:	gcc
BuildRequires:	gdb
%description -n python3-aiohttp
Async http client/server framework (asyncio).

%package help
Summary:	Development documents and examples for aiohttp
Provides:	python3-aiohttp-doc
%description help
Development documents and examples for aiohttp.

%prep
%autosetup -n aiohttp-3.7.4.post0

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-aiohttp -f filelist.lst
%dir %{python3_sitearch}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Fri Jul 23 2021 wutao <wutao61@huawei.com> - 3.7.4-1
- Package init
